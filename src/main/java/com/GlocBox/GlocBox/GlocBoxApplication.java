package com.GlocBox.GlocBox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlocBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlocBoxApplication.class, args);
	}

}
