package com.GlocBox.GlocBox.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Box {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer numeroBox;
    @ManyToOne
    private Locataire loactaire;
    @ManyToOne
    private Garage garage;
    private BigDecimal montantLoyer;
    private BigDecimal montantCharges;
    @ManyToMany
    @JoinTable(
            name = "Garer",
            joinColumns = @JoinColumn(name = "numeroBox"),
            inverseJoinColumns = @JoinColumn(name = "vehicule_immetriculation")
    )
    private List<Vehicule> vehicules;


}
